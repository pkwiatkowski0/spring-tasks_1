package pl.sda.enums;

/**
 * Created by patry on 12.03.2017.
 */
public enum Continent {
    EUROPE, ASIA, AFRICA;
}

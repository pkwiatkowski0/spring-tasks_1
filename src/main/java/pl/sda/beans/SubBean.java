package pl.sda.beans;

import java.util.List;

/**
 * Created by patry on 11.03.2017.
 */
public class SubBean {
    private String name;

    public SubBean(String name) {
        this.name = name;
    }

    public SubBean() {
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}

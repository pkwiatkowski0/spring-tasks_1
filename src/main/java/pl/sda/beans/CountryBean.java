package pl.sda.beans;

import pl.sda.enums.Continent;

/**
 * Created by patry on 11.03.2017.
 */
public class CountryBean {
    private String name;
    private String isoCode;
    private Continent continent;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIsoCode() {
        return isoCode;
    }

    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    public Continent getContinent() {
        return continent;
    }

    public void setContinent(Continent continent) {
        this.continent = continent;
    }
}

package pl.sda.beans;

import java.util.List;
import java.util.Map;

/**
 * Created by patry on 11.03.2017.
 */
public class Bean {

    private String code;
    private BeanVersion version;
    private List<String> altCodes;
    private List<SubBean> subBeans;
    private Map<String,CountryBean> countryBeansMap;


    public List<String> getAltCodes() {
        return altCodes;
    }

    public void setAltCodes(List<String> altCodes) {
        this.altCodes = altCodes;
    }

    public Bean(String s) {
        this.code = s;
    }

    public Bean(String s, Bean bean) {

    }

    public Bean(String s, BeanVersion beanVersion) {
        this.code = s;
        this.version = beanVersion;
    }

    public Bean(String s, BeanVersion beanVersion, List altCodes) {
        this.code = s;
        this.version = beanVersion;
        this.altCodes = altCodes;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BeanVersion getVersion() {
        return version;
    }

    public void setVersion(BeanVersion version) {
        this.version = version;
    }

    public List<SubBean> getSubBeans() {
        return subBeans;
    }

    public void setSubBeans(List<SubBean> subBeans) {
        this.subBeans = subBeans;
    }

    public Map<String, CountryBean> getCountryBeansMap() {
        return countryBeansMap;
    }

    public void setCountryBeansMap(Map<String, CountryBean> countryBeansMap) {
        this.countryBeansMap = countryBeansMap;
    }
}

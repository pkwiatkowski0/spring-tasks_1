package pl.sda.beans;

/**
 * Created by patry on 11.03.2017.
 */
public class BeanVersion {

    private int versionNumber;

    public BeanVersion(int i) {
        this.versionNumber = i;
    }
    public int getVersionNumber() {
        return versionNumber;
    }
}

package pl.sda.beans;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.sda.enums.Continent;

import javax.annotation.Resource;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by patry on 11.03.2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/spring-config.xml"})
public class BeanTest {
    @Qualifier("test")
    @Resource
//    @Autowired
    private Bean bean;

    @Test
    public void test() {
        assertNotNull(bean);
        assertEquals("00001", bean.getCode());
    }

    @Test
    public void test2() {
        assertNotNull(bean);
        assertEquals(1, bean.getVersion().getVersionNumber());
    }

    @Test
    public void test4() {
        assertEquals("54000", bean.getAltCodes().get(2));
    }

    @Test
    public void getSubBeans() { //test6
        assertNotNull(bean.getSubBeans());
        assertEquals("firstSubBean", bean.getSubBeans().get(0).getName());
        assertEquals("secondSubBean", bean.getSubBeans().get(1).getName());
    }

    @Test
    public void getCountryBeans() { //test7
        assertNotNull(bean);
        Assert.assertThat(bean.getCountryBeansMap().size(), is(3));
        assertEquals("Poland", bean.getCountryBeansMap().get("Poland").getName());
        assertEquals("616", bean.getCountryBeansMap().get("Poland").getIsoCode());
        assertEquals(Continent.EUROPE, bean.getCountryBeansMap().get("Poland").getContinent());

        assertEquals("China", bean.getCountryBeansMap().get("China").getName());
        assertEquals("156", bean.getCountryBeansMap().get("China").getIsoCode());
        assertEquals(Continent.ASIA, bean.getCountryBeansMap().get("China").getContinent());

        assertEquals("Kongo", bean.getCountryBeansMap().get("Kongo").getName());
        assertEquals("178", bean.getCountryBeansMap().get("Kongo").getIsoCode());
        assertEquals(Continent.AFRICA, bean.getCountryBeansMap().get("Kongo").getContinent());
    }

    @Qualifier("childBean")
    @Resource
    private Bean childBean;

    @Test
    public void test5() {
        assertEquals("00010", childBean.getCode());
        assertEquals("15000", childBean.getAltCodes().get(1));

    }


}